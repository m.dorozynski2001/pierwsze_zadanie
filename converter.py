import cv2
from pathlib import Path
import os

path:Path  = Path("good/cranesafety/batch_10/processed/")
saving_path = "converted/"
if not os.path.exists(saving_path):
            os.makedirs(saving_path)

for file in path.iterdir():
    image_path = str(file)
    image = cv2.imread(image_path)
    image_path = Path(image_path).name
    if image_path[-4:] == ".jpg":
        image_path = image_path[:-4]
        image_path = image_path + "c.png"
    cv2.imwrite(f"{saving_path}/{image_path}",image)